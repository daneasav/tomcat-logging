#!/bin/bash

CATALINA_PROPERTIES=${CATALINA_HOME}/conf/catalina.properties

cp -f /artifacts/config/server.xml ${CATALINA_HOME}/conf/server.xml

#if a backup is present (from build time), restore it, the values will be recalculated below
if [ -f ${CATALINA_PROPERTIES}.bak ]; then
	cat ${CATALINA_PROPERTIES}.bak > ${CATALINA_PROPERTIES}
else
	cat ${CATALINA_PROPERTIES} > ${CATALINA_PROPERTIES}.bak
fi

echo "connector.http.proxy.port=8080" >> ${CATALINA_PROPERTIES}
echo "connector.http.scheme=http" >> ${CATALINA_PROPERTIES}
echo "connector.http.proxy.name=" >> ${CATALINA_PROPERTIES}
echo "connector.http.port=8080" >> ${CATALINA_PROPERTIES}
echo "connector.http.redirect.port=8443" >> ${CATALINA_PROPERTIES}
echo "connector.https.keystore.path=/usr/local/tomcat/conf/tomcat-keystore.jks" >> ${CATALINA_PROPERTIES}
echo "connector.https.keystore.alias=tomcat" >> ${CATALINA_PROPERTIES}
echo "connector.https.keystore.password=changeit" >> ${CATALINA_PROPERTIES}
echo "connector.https.port=8443" >> ${CATALINA_PROPERTIES}
echo "connector.https.redirect.port=8443" >> ${CATALINA_PROPERTIES}
