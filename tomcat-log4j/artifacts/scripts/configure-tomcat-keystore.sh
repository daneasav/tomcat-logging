#!/bin/bash

if [ -f /artifacts/config/tomcat-keystore.jks ]; then
	echo "** Using existing /artifacts/config/tomcat-keystore.jks ssl keystore for the webcontainer"
	cp -f /artifacts/config/tomcat-keystore.jks ${CATALINA_HOME}/conf/
else
	echo "** Creating ssl keystore for the webcontainer"
	keytool -genkey -keyalg rsa -alias tomcat -dname "CN=localhost,OU=SWD,O=Rudolf Haufe Verlag GmbH und Co KG,L=Freiburg,ST=Baden-Wuerttemberg,C=DE" -keypass "changeit" -storepass "changeit" -keystore ${CATALINA_HOME}/conf/tomcat-keystore.jks -validity 3650
fi

