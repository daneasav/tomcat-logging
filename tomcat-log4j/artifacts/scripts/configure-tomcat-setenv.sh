#!/bin/bash

cat - > $CATALINA_HOME/bin/setenv.sh << EOF
# Edit this file to set custom options
# Tomcat accepts two parameters JAVA_OPTS and CATALINA_OPTS
# JAVA_OPTS are used during START/STOP/RUN
# CATALINA_OPTS are used during START/RUN

# JAVA_HOME is set in the environment. If not, set here by setting the
# following variable.
# JAVA_HOME=""
AGENT_PATHS=""
JAVA_AGENTS=""
JAVA_LIBRARY_PATH=""
#jvm options useful for a tomcat instance, the JVM_OPTS can be overridden from outside of this script
JVM_OPTS="-Xmx2048m -Xms128m -XX:PermSize=256m -XX:MaxPermSize=256m -server"
JVM_GC_OPTS="-XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:+CMSClassUnloadingEnabled"
JAVA_OPTS="\$JAVA_OPTS \$JVM_OPTS \$JVM_GC_OPTS \$AGENT_PATHS \$JAVA_AGENTS \$JAVA_LIBRARY_PATH"

# Make the classpath use the slf4j and logback jars during startup
# According to catalina.sh this variable is not ovverriden from this script (setenv.sh)
CLASSPATH=$CLASSPATH

EOF
