# Tomcat slf4j + logback

The docker image replaces the default jul implementation in Tomcat with a combination of slf4j and logback
 (http://logback.qos.ch).
Logback is really fast and wants to be a better successor to log4j.

The image also replaces the logging for access logs in Tomcat, so that logback is the provider that manages them as well.

## Purpose
Since slf4j is on the classpath, applications that don't include a logging provider and only log via slf4j,
 can benefit from a singe logging config.
This is useful for having the same message format across all applications.
Log aggregators like Fluentd or Logstash would benefit, as they only need to configure one pattern
 that is predictable across tomcat images and applications.
Logback configuration included in the image, pushes all the logs to the console so that no files are involved.

Logs pattern: %d %-4relative [%thread] %-5level %logger{35} - %msg%n
Access logs pattern: %h %l %u [%t] "%r" %s %b "%i{Referer}" "%i{User-Agent}" (also known as combined)

### Enhances
This image also provides new config options on top of the default server.xml:
1. The access logs valve was replaced with the logback implementation.
2. A new https connector is defined.
3. The http connector was enhanced with more options.

The enhances on the http and https connector were made to server.xml,
 so that the file will not be modified and the access valve config could be lost.
All the enhances use the $CATALINA_HOME/conf/catalina.properties file for variables specified in the server.xml.
The new properties (located at the end of the file) are (along with their default values):
https://tomcat.apache.org/tomcat-8-doc/config/http.html

| Property                                                               | Tomcat connector property |
|------------------------------------------------------------------------|---------------------------|
|connector.http.proxy.port=8080                                          |proxyPort                  |
|connector.http.scheme=http                                              |scheme                     |
|connector.http.proxy.name=                                              |proxyName                  |
|connector.http.port=8080                                                |port                       |
|connector.http.redirect.port=8443                                       |redirectPort               |
|connector.https.keystore.path=/usr/local/tomcat/conf/tomcat-keystore.jks|keystoreFile               |
|connector.https.keystore.alias=tomcat                                   |keyAlias                   |
|connector.https.keystore.password=changeit                              |keyPass                    |
|connector.https.port=8443                                               |port                       |
|connector.https.redirect.port=8443                                      |redirectPort               |

Modify catalina.properties values as you see fit.

### Environment properties

The setenv.sh script is also generated so that logback can be set on the tomcat classpath.
The setenv.sh script doesn't come with tomcat by default, but it is read if it exists by the catalina.sh script.
The file contains several variables that can be overridden with environment properties.

Below you can check the default values.
JVM_OPTS="-Xmx2048m -Xms128m -XX:PermSize=256m -XX:MaxPermSize=256m -server"
JVM_GC_OPTS="-XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:+CMSClassUnloadingEnabled"

## Usage

